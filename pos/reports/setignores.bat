@echo off
:: $Id$
:: $URL$
setlocal
setlocal enableextensions
IF ERRORLEVEL 1 echo Unable to enable extensions
pushd %~dp0
set _MY_TEMP=%temp%\~%~n0%RANDOM%.tmp
dir /ad /b /s | find /V ".svn">%_MY_TEMP%
FOR /f %%i IN ('type %_MY_TEMP%') DO (
  svn ps svn:ignore -F ignores.txt %%i
)
del %_MY_TEMP%
pause
