<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="ShippingDocument" pageWidth="612" pageHeight="792" columnWidth="535" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<reportFont name="title" isDefault="false" fontName="Arial" size="20" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
	<reportFont name="normal" isDefault="false" fontName="Arial" size="12" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
	<reportFont name="normal-bold" isDefault="false" fontName="Arial" size="12" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
	<reportFont name="detail" isDefault="false" fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
	<parameter name="HELPER" class="dtv.pos.reporting.fill.IReportHelper" isForPrompting="false"/>
	<parameter name="ShippingDocumentDetail" class="net.sf.jasperreports.engine.JasperReport" isForPrompting="false"/>
	<parameter name="logo" class="java.io.InputStream" isForPrompting="false"/>
	<parameter name="ShippingDocumentSerialLineItems" class="net.sf.jasperreports.engine.JasperReport" isForPrompting="false"/>
	<field name="documentId" class="java.lang.String"/>
	<field name="documentTypeCode" class="java.lang.String"/>
	<field name="expectedDeliveryDate" class="java.util.Date"/>
	<field name="actualDeliveryDate" class="java.util.Date"/>
	<field name="expectedShipDate" class="java.util.Date"/>
	<field name="actualShipDate" class="java.util.Date"/>
	<field name="destinationName" class="java.lang.String"/>
	<field name="destinationParty" class="java.lang.Object"/>
	<field name="destinationRetailLocation" class="java.lang.Object"/>
	<field name="address" class="java.lang.Object"/>
	<field name="shippingCarrier" class="java.lang.String"/>
	<field name="trackingNumber" class="java.lang.String"/>
	<field name="shipmentStatusCode" class="java.lang.String"/>
	<field name="parentDocument" class="java.lang.Object"/>
	<field name="shipmentLineItems" class="java.lang.Object"/>
	<field name="objectId" class="java.lang.Object"/>
	<variable name="ParentDocument" class="dtv.xst.dao.inv.IInventoryControlDocument" resetType="Group" resetGroup="shipment">
		<variableExpression><![CDATA[((dtv.xst.dao.inv.IInventoryControlDocument)$F{parentDocument})]]></variableExpression>
		<initialValueExpression><![CDATA[((dtv.xst.dao.inv.IInventoryControlDocument)$F{parentDocument})]]></initialValueExpression>
	</variable>
	<variable name="Address" class="dtv.util.address.IAddress" resetType="Group" resetGroup="shipment">
		<variableExpression><![CDATA[$F{address} != null
 ?((dtv.util.address.IAddress)$F{address}).getAddress1() != null 
     ? ((dtv.util.address.IAddress)$F{address})
     : ((dtv.xst.dao.loc.IRetailLocation)$F{destinationRetailLocation})
 :((dtv.xst.dao.loc.IRetailLocation)$F{destinationRetailLocation})]]></variableExpression>
	</variable>
	<group name="shipment">
		<groupExpression><![CDATA[$F{objectId}]]></groupExpression>
		<groupHeader>
			<band splitType="Stretch"/>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch"/>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band height="163" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="110" y="72" width="189" height="15" forecolor="#000000" backcolor="#FFFFFF">
					<printWhenExpression><![CDATA[new Boolean ($V{ParentDocument}.getOriginatorId() != null)]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single" markup="none">
					<font reportFont="normal"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Store#: "+$V{ParentDocument}.getOriginatorId()]]></textFieldExpression>
			</textField>
			<image scaleImage="RetainShape" hAlign="Left" vAlign="Top" isUsingCache="false" evaluationTime="Report">
				<reportElement key="image" mode="Opaque" x="387" y="30" width="162" height="50" forecolor="#000000" backcolor="#FFFFFF"/>
				<graphicElement fill="Solid"/>
				<imageExpression class="java.awt.Image"><![CDATA[$P{HELPER}.getBarcode($F{documentId},"Code 93","BELOW")]]></imageExpression>
			</image>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="387" y="0" width="162" height="30" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
					<font reportFont="title"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_shippingReportPackingSlip")]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="387" y="78" width="162" height="19" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
					<font reportFont="normal-bold"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$V{ParentDocument}.getDocumentSubtypeCode()]]></textFieldExpression>
			</textField>
			<image scaleImage="RetainShape" hAlign="Left" vAlign="Top" isUsingCache="false" evaluationTime="Report">
				<reportElement key="image" mode="Opaque" x="387" y="98" width="162" height="43" forecolor="#000000" backcolor="#FFFFFF"/>
				<graphicElement fill="Solid"/>
				<imageExpression class="java.awt.Image"><![CDATA[$P{HELPER}.getBarcode($V{ParentDocument}.getControlNumber(),"Code 93","BELOW")]]></imageExpression>
			</image>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="387" y="141" width="162" height="19" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
					<font reportFont="normal-bold"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$V{ParentDocument}.getControlNumber() == null || "".equals($V{ParentDocument}.getControlNumber().trim())
 ? ""
 : $P{HELPER}.translate("_shippingReportControlNumber")]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="1" y="72" width="109" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single" markup="none">
					<font reportFont="normal-bold"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_shippingReportShipFrom")]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" evaluationTime="Auto" pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="1" y="102" width="300" height="15" isRemoveLineWhenBlank="true" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single" markup="none">
					<font reportFont="normal"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.formatAddress($V{ParentDocument}.getOriginatorAddress())]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="1" y="87" width="300" height="15" isRemoveLineWhenBlank="true" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single" markup="none">
					<font reportFont="normal"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$V{ParentDocument}.getOriginatorName()]]></textFieldExpression>
			</textField>
			<image scaleImage="RetainShape">
				<reportElement x="0" y="0" width="165" height="117"/>
				<imageExpression class="java.lang.String"><![CDATA["res/graphics/receipt/logo-cri.bmp"]]></imageExpression>
				<!--<imageExpression class="java.lang.String"><![CDATA["C:/Workspace-5.0.2/cri_pos/res/graphics/receipt/logo-cri.bmp"]]></imageExpression>-->
			</image>
		</band>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="512" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="0" y="6" width="109" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single" markup="none">
					<font reportFont="normal-bold"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_shippingReportShipTo")]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" evaluationTime="Auto" pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="0" y="36" width="300" height="62" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single" markup="none">
					<font reportFont="normal"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.formatAddress($V{Address})]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="0" y="21" width="300" height="15" isRemoveLineWhenBlank="true" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single" markup="none">
					<font reportFont="normal"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{destinationName}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="306" y="21" width="102" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
					<font reportFont="normal"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_shippingReportOrderDate")]]></textFieldExpression>
			</textField>
			<textField pattern="MMMM d, yyyy" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="410" y="21" width="140" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
					<font reportFont="normal"/>
				</textElement>
				<textFieldExpression class="java.util.Date"><![CDATA[$V{ParentDocument}.getCreateDateTime()]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="306" y="36" width="102" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
					<font reportFont="normal"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_shippingReportOrderNumber")]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="410" y="36" width="140" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
					<font reportFont="normal"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{documentId}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="306" y="51" width="102" height="15" isRemoveLineWhenBlank="true" forecolor="#000000" backcolor="#FFFFFF">
					<printWhenExpression><![CDATA[Boolean.valueOf(($V{ParentDocument}.getPoReferenceNumber() != null) && ($V{ParentDocument}.getPoReferenceNumber().length() != 0))]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
					<font reportFont="normal"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_shippingReportPurchaseOrder")]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="410" y="51" width="140" height="15" isRemoveLineWhenBlank="true" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
					<font reportFont="normal"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$V{ParentDocument}.getPoReferenceNumber()]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="306" y="6" width="102" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
					<font reportFont="normal"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_shippingReportDate")]]></textFieldExpression>
			</textField>
			<textField pattern="MMMM d, yyyy" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="410" y="6" width="140" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
					<font reportFont="normal"/>
				</textElement>
				<textFieldExpression class="java.util.Date"><![CDATA[$F{actualShipDate}]]></textFieldExpression>
			</textField>
			<subreport isUsingCache="false">
				<reportElement key="subreport" positionType="Float" mode="Opaque" x="0" y="110" width="552" height="400" forecolor="#000000" backcolor="#FFFFFF"/>
				<parametersMapExpression><![CDATA[$P{REPORT_PARAMETERS_MAP}]]></parametersMapExpression>
				<subreportParameter name="ShippingDocumentSerialLineItems">
					<subreportParameterExpression><![CDATA[$P{ShippingDocumentSerialLineItems}]]></subreportParameterExpression>
				</subreportParameter>
				<dataSourceExpression><![CDATA[new net.sf.jasperreports.engine.data.JRBeanCollectionDataSource((java.util.List)$F{shipmentLineItems})]]></dataSourceExpression>
				<subreportExpression class="net.sf.jasperreports.engine.JasperReport"><![CDATA[$P{ShippingDocumentDetail}]]></subreportExpression>
			</subreport>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="410" y="67" width="140" height="15" isRemoveLineWhenBlank="true" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
					<font reportFont="normal"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{shippingCarrier}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="410" y="83" width="140" height="15" isRemoveLineWhenBlank="true" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
					<font reportFont="normal"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{trackingNumber}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="306" y="67" width="102" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
					<font reportFont="normal"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_shippingReportShippingCarrier")]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="306" y="83" width="102" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
					<font reportFont="normal"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_shippingReportTrackingNumber")]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="109" y="5" width="188" height="15" forecolor="#000000" backcolor="#FFFFFF">
					<printWhenExpression><![CDATA[new Boolean(((dtv.xst.dao.loc.IRetailLocation)$F{destinationRetailLocation}).getStoreNbr() != null)]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single" markup="none">
					<font reportFont="normal"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Store#: "+((dtv.xst.dao.loc.IRetailLocation)$F{destinationRetailLocation}).getStoreNbr()]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="15" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" positionType="Float" mode="Opaque" x="165" y="0" width="122" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" lineSpacing="Single">
					<font reportFont="normal"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{HELPER}.translate("_rptPage")+" " +$V{PAGE_NUMBER}+" " + $P{HELPER}.translate("_rptof") + " "]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="" isBlankWhenNull="true">
				<reportElement key="textField" positionType="Float" mode="Opaque" x="287" y="0" width="122" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
					<font reportFont="normal"/>
				</textElement>
				<textFieldExpression class="java.lang.Integer"><![CDATA[$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band height="24" splitType="Stretch"/>
	</summary>
</jasperReport>
