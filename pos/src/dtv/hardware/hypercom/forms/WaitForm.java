//$Id$
package dtv.hardware.hypercom.forms;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author aabdul
 * @created Aug 3, 2012
 * @version $Revision$
 */
public class WaitForm
    extends AbstractDeviceForm {

  /**
   * Constructs a <code>WaitForm</code>.
   * @param argFormId
   */
  public WaitForm() {
    super("FNWAITFRM");
  }
}
