//$Id$
package cri.pos.isd.km;

import static dtv.util.StringUtils.isEmpty;

import dtv.pos.framework.ui.op.AbstractPromptOp;
import dtv.pos.iframework.event.IXstEvent;
import dtv.pos.iframework.op.IOpResponse;
import dtv.pos.iframework.op.IXstCommand;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2012 MICROS Retail
 * 
 * @author aabdul
 * @created Sep 26, 2012
 * @version $Revision$
 */
public class CriIsdKMKeyPromptOp
    extends AbstractPromptOp {

  private static final long serialVersionUID = 1L;

  /** {@inheritDoc} */
  @Override
  protected IOpResponse handlePromptResponse(IXstCommand argCmd, IXstEvent argEvent) {
    return HELPER.silentErrorResponse();
  }

  @Override
  public boolean isOperationApplicable(IXstCommand argCmd) {
    return isISDKeyNotLoaded();

  }

  private boolean isISDKeyNotLoaded() {
    ISDKeyManagementHelper kmHelper = ISDKeyManagementHelper.getInstance();
    return isEmpty(kmHelper.getKeyId()) || isEmpty(kmHelper.getCurrKeyValue());
  }
}
