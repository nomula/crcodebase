//$Id$
package cri.pos.register;

/**
 * Customer value key container class.<br>
 * <br>
 * Copyright (c) 2016 MICROS Retail
 *
 * @author johgaug
 * @created Sep 29, 2016
 * @version $Revision$
 */
public final class CriValueKey {

  /** Accesses a flag determining whether to prompt for a customer. */
  public static final ValueKey<Boolean> PROMPT_CUSTOMER =
      new ValueKey<Boolean>(Boolean.class, "CRI_PROMPT_CUSTOMER");

  /** Prevent instantiation: this class is used as a namespace only. */
  private CriValueKey() {
    throw new UnsupportedOperationException();
  }

}
