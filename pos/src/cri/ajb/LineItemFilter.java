//$Id$
package cri.ajb;

import dtv.pos.register.DefaultLineItemFilter;
import dtv.xst.dao.trl.IRetailTransactionLineItem;
import dtv.xst.dao.ttr.ITenderLineItem;

/**
 * Determines what lines show up on the line display of a signature capture device.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision$
 */
public class LineItemFilter
    extends DefaultLineItemFilter.Filter {

  /** {@inheritDoc} */
  @Override
  protected boolean includeLine(IRetailTransactionLineItem argLine) {
    // exclude tender lines
    if (argLine instanceof ITenderLineItem) {
      return false;
    }
    return super.includeLine(argLine);
  }
}