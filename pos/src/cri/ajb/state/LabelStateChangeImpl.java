//$Id$
package cri.ajb.state;

/**
 * Label state change impl.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision$
 */
public class LabelStateChangeImpl
    extends AbstractStateChange {

  /**
   * Constructor method.
   * 
   * @param argNewText
   */
  protected LabelStateChangeImpl(String argNewText) {
    super(argNewText);
  }
}
