//$Id$
package cri.ajb.state;

/**
 * State change.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision$
 */
public abstract class AbstractStateChange
    implements IStateChange {

  private final String newText_;

  /**
   * Constructor method.
   *
   * @param argNewText
   */
  protected AbstractStateChange(String argNewText) {
    newText_ = argNewText;
  }

  /** {@inheritDoc} */
  @Override
  public String getNewText() {
    return newText_;
  }

}