//$Id$
package cri.ajb.state;

/**
 * State change type.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision$
 */
public enum ChangeType {
  /* * * * * * * * * * START OF INSTANCES * * * * * * * * * */
  /** the item is new */
  NEW, /** the item was removed */
  DELETED, /** the item is modified */
  MODIFIED;
  /* * * * * * * * * *  END OF INSTANCES  * * * * * * * * * */

}
