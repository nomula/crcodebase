//$Id$
package cri.ajb.fipay.reqeust;

/**
 * AJB tender auth request.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision$
 */
public interface IAjbFiPayTenderAuthRequest {

  /**
   * Get the AJB command.
   * 
   * @return
   */
  String getCommand();
}
