//$Id$
package cri.ajb.fipay.reqeust;

import dtv.tenderauth.AuthRequestType;

/**
 * AJB FiPay IDLE screen display request.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision$
 */
public class AjbFiPayIdleDisplayRequest
    extends AjbFiPayTransactionDisplayRequest {
  /**
   * Constructor method
   * 
   * @param argType
   * @param argData
   */
  public AjbFiPayIdleDisplayRequest(AuthRequestType argType, Object[] argData) {
    super(argType, argData);

    //clean fields.
    setField(HEADER_NBR, null);
    setField(SUMMARY_NBR, null);
  }
}
