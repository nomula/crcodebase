//$Id$
package cri.ajb.fipay.response;

/**
 * AJB signature capture response.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision$
 */
public interface IAjbFiPaySignatureCaptureResponse {

  /**
   * Get the the signature data.
   * 
   * @return
   */
  public String getSignature();
}
