//$Id$
package cri.ajb.fipay.response;

import dtv.tenderauth.IAuthRequest;
import dtv.tenderauth.impl.ajb.response.AbstractAjbResponse;

/**
 * AJB generic response.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author Shy Xie
 * @created Jun 17, 2015
 * @version $Revision$
 */
public class AjbFiPayGenericResponse
    extends AbstractAjbResponse {

  /**
   * 
   */
  private static final long serialVersionUID = -2309673731233478974L;

  /**
   * Constructor method.
   * 
   * @param argRequest
   * @param argFields
   */
  public AjbFiPayGenericResponse(IAuthRequest argRequest, String[] argFields) {
    super(argRequest, argFields, null);
  }

  /** {@inheritDoc} */
  @Override
  public String getResponseCode() {
    return getField(MESSAGE_ID);
  }
}
