// $Id$
package cri.xst.dao.ttr;

/**
 * DTX extension interface.<br>
 * <br>
 * Copyright (c) 2015 MICROS Retail
 * 
 * @author shy xie
 * @created June 30, 2015
 * @version $Revision$
 */
public interface ICriTokenUpdateQueueModel {

  /**
   * Get the related credit tender line item.
   * 
   * @return
   */
  public dtv.xst.dao.ttr.ICreditDebitTenderLineItem getCreditDebitTenderLineItem();
}
