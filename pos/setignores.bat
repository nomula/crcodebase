@echo off
:: $Id$
:: $URL$
setlocal
setlocal enableextensions
IF ERRORLEVEL 1 echo Unable to enable extensions
pushd %~dp0

svn ps svn:ignore -F ignores.txt .

echo *>~all-ignores.tmp
svn ps svn:ignore -F ~all-ignores.tmp gen
svn ps svn:ignore -F ~all-ignores.tmp log
del ~all-ignores.tmp

call reports\setignores.bat
