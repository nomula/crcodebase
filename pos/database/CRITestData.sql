PRINT '$Id$';
PRINT '$URL$';

-- ************************************************************************************************
--
-- This script should contain dummy data used only in a test environment such as items, deals and
-- customers.
--
-- ************************************************************************************************

DECLARE @intOrganization_ID INT;
SET @intOrganization_ID = $(OrgID);
DECLARE @strCountry_ID VARCHAR(2);
SET @strCountry_ID = $(CountryID);
DECLARE @intStore_ID INT;
SET @intStore_ID = $(StoreID);
DECLARE @strCurrency_ID VARCHAR(3);
SET @strCurrency_ID = $(CurrencyID);

DECLARE @Price MONEY;

GO

