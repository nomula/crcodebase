package dtv.tenderauth.impl;

import java.util.Collection;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import dtv.hardware.auth.ICommCallback;
import dtv.hardware.auth.UserCancelledException;
import dtv.i18n.IFormattable;
import dtv.tenderauth.*;
import dtv.tenderauth.event.IAuthResponse;
import dtv.tenderauth.impl.event.AuthStatus;
import dtv.tenderauth.impl.event.AuthStatusUpdater;
import dtv.util.*;
import dtv.xst.dao.ttr.ICreditDebitTenderLineItem;

public abstract class AbstractCommunicator
    implements IAuthCommunicator, dtv.util.IHasParameters {
  private static final boolean IS_DEVELOPER = false;
  private static final Logger logger_ = Logger.getLogger(AbstractCommunicator.class);
  private static final Logger adminLogger_ = Logger.getLogger("dtv.xstore.comm.paysys");

  protected static final String MESSAGE_ENCODING = "US-ASCII";

  private static final int DEFAULT_CONNECT_MILLIS = 5000;
  private static final int DEFAULT_SENDING_MILLIS = 1000;
  private static final int DEFAULT_WAIT_MILLIS = 60000;
  private static final int DEFAULT_RECEIVE_MILLIS = 1000;

  protected static boolean toBoolean(Object argValue) {
    if ((argValue instanceof Boolean)) {
      return ((Boolean) argValue).booleanValue();
    }
    if ((argValue instanceof String)) {
      return Boolean.valueOf((String) argValue).booleanValue();
    }
    logger_.warn("unexpected value for boolean " + ObjectUtils.getClassNameFromObject(argValue) + "::"
        + argValue);

    return false;
  }

  protected static int toInteger(Object argValue) {
    if ((argValue instanceof Integer)) {
      return ((Integer) argValue).intValue();
    }
    if ((argValue instanceof String)) {
      return Integer.valueOf((String) argValue).intValue();
    }
    logger_.warn("unexpected value for integer " + ObjectUtils.getClassNameFromObject(argValue) + "::"
        + argValue);

    return 0;
  }

  public AbstractCommunicator() {
    progressConnectMillis_ = 5000;
    progressSendingMillis_ = 1000;

    progressWaitMillis_ = 60000;

    progressReceiveMillis_ = 1000;

    receiveTimeoutMillis_ = 120000;
    progressConnectPercent_ = 10;
    progressSendingPercent_ = 30;

    progressWaitPercent_ = 90;
    progressReceivePercent_ = 100;

    commAborter_ = new DefaultCommCallback();

    request_ = new ThreadLocal<IAuthRequest>();
  }

  public final void abortCommunications() {
    commAborter_.abortCommunications();
  }

  private static final int DEFAULT_RECEIVE_TIMEOUT_MILLIS = 120000;
  private int progressConnectMillis_;
  private int progressSendingMillis_;

  public ICommCallback getCommCallback() {
    return commAborter_;
  }

  private int progressWaitMillis_;
  private int progressReceiveMillis_;
  private int receiveTimeoutMillis_;

  public final IAuthResponse sendRequest(IAuthRequest argRequest)
      throws UserCancelledException, ReceiveTimeoutException {
    try {
      request_.set(argRequest);
      return sendRequestImpl();
    }
    finally {
      request_.remove();
    }
  }

  public final void setAuthProcess(IAuthProcess argAuthProcess) {
    authProcess_ = argAuthProcess;
  }

  public void setParameter(String argName, Object argValue) {
    if (((argValue instanceof List)) && ("communicatorHosts".equals(argName))) {
      try {
        List<?> l = (List) argValue;
        String[] hosts = new String[l.size()];
        for (int i = 0; i < l.size(); i++ ) {
          hosts[i] = l.get(i).toString();
        }
        hosts_ = hosts;
      }
      catch (Exception ex) {
        logger_.error("CAUGHT EXCEPTION trying to convert to String[]", ex);
      }
    }
    else if ("communicatorHost".equals(argName)) {
      hosts_ = new String[] {argValue.toString()};
      logger_.warn("configuring a single communicatorHost is deprecated and will be removed");
    }
    else if ("communicatorReceiveTimeoutMillis".equals(argName)) {
      receiveTimeoutMillis_ = toInteger(argValue);
    }
    else if ("communicatorProgressConnectMillis".equals(argName)) {
      progressConnectMillis_ = toInteger(argValue);
    }
    else if ("communicatorProgressSendingMillis".equals(argName)) {
      progressSendingMillis_ = toInteger(argValue);
    }
    else if ("communicatorProgressWaitMillis".equals(argName)) {
      progressWaitMillis_ = toInteger(argValue);
    }
    else if ("communicatorProgressReceiveMillis".equals(argName)) {
      progressReceiveMillis_ = toInteger(argValue);
    }
  }

  public void setRequestConverter(IAuthRequestConverter argConverter) {
    requestConverter_ = argConverter;
  }

  public void setResponseConverter(IAuthResponseConverter argConverter) {
    responseConverter_ = argConverter;
  }

  protected void calculateProgressPercents() {
    int totalTime =
        progressConnectMillis_ + progressSendingMillis_ + progressWaitMillis_ + progressReceiveMillis_;

    progressConnectPercent_ = (100 * progressConnectMillis_ / totalTime);
    if (progressConnectPercent_ == 0) {
      progressConnectPercent_ = 1;
    }
    progressSendingPercent_ = (progressConnectPercent_ + 100 * progressSendingMillis_ / totalTime);
    if (progressSendingPercent_ == progressConnectPercent_) {
      progressSendingPercent_ = (progressConnectPercent_ + 1);
    }
    progressWaitPercent_ = (progressSendingPercent_ + 100 * progressWaitMillis_ / totalTime);
    if (progressWaitPercent_ == progressSendingPercent_) {
      progressWaitPercent_ = (progressSendingPercent_ + 1);
    }
    progressReceivePercent_ = 100;
  }

  protected void checkTimeout(long argTimeoutTime)
      throws ReceiveTimeoutException {
    if (System.currentTimeMillis() > argTimeoutTime) {
      throw new ReceiveTimeoutException();
    }
  }

  protected void checkUserCancelled()
      throws UserCancelledException {
    commAborter_.checkUserCancelled();
  }

  protected String cleanLog(String argValue) {
    if (IS_DEVELOPER) {
      return argValue;
    }
    if (argValue == null) {
      logger_.error("null event or null message?!?!?!");
      return "";
    }
    return AuthLogMasker.getInstance().mask(argValue);
  }

  protected IAuthProcess getAuthProcess() {
    return authProcess_;
  }

  protected HostList getHostList() {
    return new HostList(hosts_);
  }

  protected String getPayload(IAuthRequest argRequest) {
    return (String) getRequestConverter().convertRequest(argRequest);
  }

  protected int getProgressConnectMillis() {
    return progressConnectMillis_;
  }

  protected int getProgressConnectPercent() {
    return progressConnectPercent_;
  }

  protected int getProgressReceiveMillis() {
    return progressReceiveMillis_;
  }

  protected int getProgressReceivePercent() {
    return progressReceivePercent_;
  }

  protected int getProgressSendingMillis() {
    return progressSendingMillis_;
  }

  protected int getProgressSendingPercent() {
    return progressSendingPercent_;
  }

  protected int getProgressWaitMillis() {
    return progressWaitMillis_;
  }

  protected int getProgressWaitPercent() {
    return progressWaitPercent_;
  }

  protected int getReceiveTimeoutMillis() {
    return receiveTimeoutMillis_;
  }

  protected final IAuthRequest getRequest() {
    return request_.get();
  }

  protected IAuthRequestConverter getRequestConverter() {
    return requestConverter_;
  }

  protected IAuthResponseConverter getResponseConverter() {
    return responseConverter_;
  }

  protected AuthStatusUpdater getStatusUpdater() {
    return statusUpdater_;
  }

  protected void log(Object argMessage) {
    log(argMessage, Level.INFO);
  }

  protected void log(Object argMessage, Level argLevel) {
    StringBuilder cleanMessage = new StringBuilder(cleanLog(argMessage.toString()));
    authProcess_.getAuthLog().log(argLevel, cleanMessage.toString());
    if (argLevel.isGreaterOrEqual(Level.WARN)) {
      adminLogger_.log(argLevel, cleanMessage.toString());
    }
    AuthFactory.getInstance().addRecentCommunication(cleanMessage.toString());
    cleanMessage.delete(0, cleanMessage.length());
  }

  protected void log(Object argMessage, Throwable argThrowable) {
    StringBuilder cleanMessage = new StringBuilder(cleanLog(argMessage.toString()));
    authProcess_.getAuthLog().error(cleanMessage.toString(), argThrowable);
    adminLogger_.error(cleanMessage.toString());
    AuthFactory.getInstance().addRecentCommunication(
        new MessageWithThrowable(cleanMessage.toString(), argThrowable));

    cleanMessage.delete(0, cleanMessage.length());
  }

  private int progressConnectPercent_;
  private int progressSendingPercent_;
  private int progressWaitPercent_;
  private int progressReceivePercent_;
  private String[] hosts_;

  protected void setStatus(IAuthRequest argRequest, IFormattable argMessage, int argExpectedWaitMillis,
      int argStartPercent, int argEndPercent) {
    if (statusUpdater_ != null) {
      statusUpdater_.requestStop();
      statusUpdater_ = null;
    }

    if (argStartPercent < 0) {
      argRequest.notifyStatusListeners(new AuthStatus(argMessage, -1));
    }
    else {
      statusUpdater_ =
          new AuthStatusUpdater(argRequest, argMessage, argExpectedWaitMillis, argStartPercent, argEndPercent);

      statusUpdater_.start();
    }
  }

  AuthStatusUpdater statusUpdater_;

  protected void setStatusUpdater(AuthStatusUpdater argStatusUpdater) {
    statusUpdater_ = argStatusUpdater;
  }

  private IAuthResponseConverter responseConverter_;
  private IAuthRequestConverter requestConverter_;
  private IAuthProcess authProcess_;
  private final DefaultCommCallback commAborter_;
  private ThreadLocal<IAuthRequest> request_;

  protected class DefaultCommCallback
      implements ICommCallback {
    private final Logger _logger = Logger.getLogger(DefaultCommCallback.class);
    private boolean communicationsAborted_ = false;
    private final Collection<Thread> registered_ = WeakCollections.makeWeakSet();

    protected DefaultCommCallback() {}

    public synchronized void abortCommunications() {
      communicationsAborted_ = true;
      if (statusUpdater_ != null) {
        try {
          statusUpdater_.requestStop();
        }
        catch (Exception ex) {
          log("CAUGHT EXCEPTION", ex);
          _logger.error("CAUGHT EXCEPTION", ex);
        }
      }
      synchronized (registered_) {
        boolean interruptThisThread = false;
        for (Thread t : registered_) {
          if (t == Thread.currentThread()) {
            interruptThisThread = true;
          }
          else {
            t.interrupt();
          }
        }
        if (interruptThisThread) {
          Thread.currentThread().interrupt();
        }
      }
    }

    public synchronized void checkUserCancelled()
        throws UserCancelledException {
      if (communicationsAborted_) {
        throw new UserCancelledException();
      }
    }

    public dtv.util.IValueContainer getConfig() {
      return getAuthProcess().getParameters();
    }

    public void log(Object argMessage) {
      AbstractCommunicator.this.log(argMessage);
    }

    public void log(Object argMessage, Throwable argThrowable) {
      AbstractCommunicator.this.log(argMessage, argThrowable);
    }

    public void registerForInterruptOnUserCancel(Thread argThread) {
      synchronized (registered_) {
        registered_.add(argThread);
        if ((communicationsAborted_) && (!argThread.isInterrupted())) {
          argThread.interrupt();
        }
      }
    }

    public void setCancelAllowed(boolean argValue) {
      IAuthRequest request = getRequest();
      if (request != null) {
        request.setCancelAllowed(argValue);
      }
    }

    public synchronized void setNotCancelled() {
      communicationsAborted_ = false;
    }

    public void setStatus(IFormattable argMessage, int argExpectedWaitMillis, int argStartPercent, int argEndPercent) {
      AbstractCommunicator.this.setStatus(getRequest(), argMessage, argExpectedWaitMillis, argStartPercent, argEndPercent);
    }

    public void unregisterForInterruptOnUserCancel(Thread argThread) {
      synchronized (registered_) {
        registered_.remove(argThread);
      }
    }

    public void updateLineItem(ICreditDebitTenderLineItem argLineItem, byte[] argResponse) {
      AbstractCommunicator.this.updateLineItem(argLineItem, argResponse);
    }
  }

  protected abstract IAuthResponse sendRequestImpl()
      throws UserCancelledException, ReceiveTimeoutException;

  protected void updateLineItem(ICreditDebitTenderLineItem argLineItem, byte[] argResponse) {}
}
