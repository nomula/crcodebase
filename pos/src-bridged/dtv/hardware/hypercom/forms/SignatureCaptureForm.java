//$Id$
package dtv.hardware.hypercom.forms;

/**
 * DESCRIPTION GOES HERE<br>
 * <br>
 * Copyright (c) 2009 MICROS Retail
 * 
 * @author czhou
 * @created Jul 9, 2009
 * @version $Revision$
 */
public class SignatureCaptureForm
    extends AbstractDeviceForm {

  public SignatureCaptureForm() {
    super("FNSIGNATURE");
  }

}
