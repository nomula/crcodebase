// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fullnames braces deadcode fieldsfirst ansi nonlb space lnc radix(16) safe 
// Source File Name:   LineDisplayService.java

package com.hypercom.fpe.jpos.linedisplay;

import jpos.JposException;

public class LineDisplayService
    extends com.hypercom.fpe.jpos.FPEPosService
    implements jpos.services.LineDisplayService18 {

  private int cursorColumn;
  private int cursorRow;
  private boolean cursorUpdate;
  private int deviceBrightness;
  private int interCharacterWait;
  private int marqueeFormat;
  private int marqueeRepeatWait;
  private int marqueeUnitWait;
  private boolean mapCharacterSet;
  private int blinkRate;
  private int foregroundColor;
  private int backgroundColor;
  private com.hypercom.fpe.consts.LineDisplayFontSize fontSize;
  byte lineBuf[][];

  public LineDisplayService() {
    /*  40*/cursorColumn = 0;
    /*  41*/cursorRow = 0;
    /*  42*/cursorUpdate = true;
    /*  43*/deviceBrightness = 0x64;
    /*  44*/interCharacterWait = 0;
    /*  45*/marqueeFormat = 1;
    /*  46*/marqueeRepeatWait = 0;
    /*  47*/marqueeUnitWait = 0;
    /*  48*/mapCharacterSet = false;
    /*  49*/blinkRate = 0x3de;
    /*  52*/foregroundColor = 0;
    /*  53*/backgroundColor = 0xff;
    /*  54*/fontSize = com.hypercom.fpe.consts.LineDisplayFontSize.SMALLEST;
  }

  public com.hypercom.fpe.jpos.DeviceType getDeviceType() {
    /*  67*/return com.hypercom.fpe.jpos.DeviceType.LINE_DISPLAY;
  }

  public java.lang.String getDeviceServiceDescription() {
    /*  71*/return "LineDisplay. Hypercom Corp.";
  }

  public boolean getCapBitmap()
      throws jpos.JposException {
    /*  75*/return false;
  }

  public boolean getCapScreenMode()
      throws jpos.JposException {
    /*  79*/return false;
  }

  public boolean getCapMapCharacterSet()
      throws jpos.JposException {
    /*  83*/return false;
  }

  public boolean getMapCharacterSet()
      throws jpos.JposException {
    /*  87*/return mapCharacterSet;
  }

  public void setMapCharacterSet(boolean mapCharacterSetP)
      throws jpos.JposException {
    /*  91*/mapCharacterSet = mapCharacterSetP;
  }

  public int getMaximumX()
      throws jpos.JposException {
    /*  95*/return 0;
  }

  public int getMaximumY()
      throws jpos.JposException {
    /*  99*/return 0;
  }

  public int getScreenMode()
      throws jpos.JposException {
    /* 103*/return 0;
  }

  public void setScreenMode(int screenMode)
      throws jpos.JposException {
    /* 107*/if (screenMode != 0) {
      /* 108*/throw new JposException(0x6a, "Only 0 screen mode supported");
    }
    else {
      /* 111*/return;
    }
  }

  public java.lang.String getScreenModeList()
      throws jpos.JposException {
    /* 114*/com.hypercom.fpe.consts.TerminalType type =
        ((com.hypercom.fpe.jpos.FPEPosService) this).getTerminalType();
    /* 115*/if (((com.hypercom.fpe.consts.BaseConstant) (type))
        .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.ICE_6000)))
        || ((com.hypercom.fpe.consts.BaseConstant) (type))
            .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.L4100)))
        || ((com.hypercom.fpe.consts.BaseConstant) (type))
            .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.L4100G)))
        || ((com.hypercom.fpe.consts.BaseConstant) (type))
            .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.L4150)))
        || ((com.hypercom.fpe.consts.BaseConstant) (type))
            .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.L4150_PCI_20)))) {
      /* 118*/return "16x43";
    }
    /* 120*/if (((com.hypercom.fpe.consts.BaseConstant) (type))
        .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.L5300)))
        || ((com.hypercom.fpe.consts.BaseConstant) (type))
            .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.L5400)))) {
      /* 121*/return "25x87";
    }
    /* 123*/if (((com.hypercom.fpe.consts.BaseConstant) (type))
        .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.L4250)))) {
      /* 124*/return "9x36";
    }
    /* 126*/if (((com.hypercom.fpe.consts.BaseConstant) (type))
        .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.L4200)))) {
      /* 127*/return "9x26";
    }
    /* 129*/if (((com.hypercom.fpe.consts.BaseConstant) (type))
        .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.M4100)))) {
      /* 130*/return "16x39";
    }
    else {
      /* 132*/return "16x43";
    }
  }

  public void displayBitmap(java.lang.String fileName, int width, int alignmentX, int alignmentY)
      throws jpos.JposException {
    /* 137*/throw new JposException(0x6a, "Bitmaps are not supported in this implementation");
  }

  public void setBitmap(int bitmapNumber, java.lang.String fileName, int width, int alignmentX, int alignmentY)
      throws jpos.JposException {
    /* 143*/throw new JposException(0x6a, "Bitmaps are not supported in this implementation");
  }

  public boolean getCapBlinkRate()
      throws jpos.JposException {
    /* 148*/return true;
  }

  public int getCapCursorType()
      throws jpos.JposException {
    /* 152*/return 0;
  }

  public boolean getCapCustomGlyph()
      throws jpos.JposException {
    /* 156*/return false;
  }

  public int getCapReadBack()
      throws jpos.JposException {
    /* 160*/return 0;
  }

  public int getCapReverse()
      throws jpos.JposException {
    /* 164*/return 0;
  }

  public int getBlinkRate()
      throws jpos.JposException {
    /* 168*/return blinkRate;
  }

  public void setBlinkRate(int blinkRateP)
      throws jpos.JposException {
    /* 172*/blinkRate = blinkRateP;
  }

  public int getCursorType()
      throws jpos.JposException {
    /* 176*/return 0;
  }

  public void setCursorType(int cursorType)
      throws jpos.JposException {
    /* 180*/if (cursorType != 0) {
      /* 181*/throw new JposException(0x6a, "Only DISP_CT_NONE cursor type is supported");
    }
    else {
      /* 184*/return;
    }
  }

  public java.lang.String getCustomGlyphList()
      throws jpos.JposException {
    /* 187*/return "";
  }

  public int getGlyphHeight()
      throws jpos.JposException {
    /* 191*/return 0;
  }

  public int getGlyphWidth()
      throws jpos.JposException {
    /* 195*/return 0;
  }

  public void defineGlyph(int glyphCode, byte glyph[])
      throws jpos.JposException {
    /* 199*/throw new JposException(0x6a, "The device does not support glyph");
  }

  public void readCharacterAtCursor(int aChar[])
      throws jpos.JposException {
    /* 204*/throw new JposException(0x6a, "The device does not support reading");
  }

  public void deleteInstance()
      throws jpos.JposException {}

  public int getCapBlink()
      throws jpos.JposException {
    /* 212*/return 2;
  }

  public boolean getCapBrightness()
      throws jpos.JposException {
    /* 216*/return false;
  }

  public int getCapCharacterSet()
      throws jpos.JposException {
    /* 220*/return 0x3e6;
  }

  public boolean getCapDescriptors()
      throws jpos.JposException {
    /* 224*/return false;
  }

  public boolean getCapHMarquee()
      throws jpos.JposException {
    /* 228*/return false;
  }

  public boolean getCapICharWait()
      throws jpos.JposException {
    /* 232*/return false;
  }

  public boolean getCapVMarquee()
      throws jpos.JposException {
    /* 236*/return false;
  }

  public int getCharacterSet()
      throws jpos.JposException {
    /* 240*/return 0x3e6;
  }

  public void setCharacterSet(int characterSetP)
      throws jpos.JposException {
    /* 244*/if (characterSetP != 0x3e6 && characterSetP != 0x3e7) {
      /* 246*/throw new JposException(0x6a, "Unsupported character set specified");
    }
    else {
      /* 250*/return;
    }
  }

  public java.lang.String getCharacterSetList()
      throws jpos.JposException {
    /* 253*/return "998,999";
  }

  public int getColumns()
      throws jpos.JposException {
    /* 257*/return getDeviceColumns();
  }

  public int getCurrentWindow()
      throws jpos.JposException {
    /* 261*/return 0;
  }

  public void setCurrentWindow(int currentWindow)
      throws jpos.JposException {
    /* 265*/if (currentWindow != 0) {
      /* 266*/throw new JposException(0x6a, "Current window can only be 0 in this device");
    }
    else {
      /* 269*/return;
    }
  }

  public int getCursorColumn()
      throws jpos.JposException {
    /* 272*/return cursorColumn;
  }

  public void setCursorColumn(int cursorColumnP)
      throws jpos.JposException {
    /* 276*/if (cursorColumnP < 0 || cursorColumnP > getColumns()) {
      /* 277*/java.lang.String msg =
          java.text.MessageFormat.format("Specified cursor column \"{0}\" is out of bounds: \"{1}-{2}\"",
              new java.lang.Object[] {
              /* 277*/java.lang.String.valueOf(cursorColumnP), "0", java.lang.String.valueOf(getColumns())});
      /* 281*/throw new JposException(0x6a, msg);
    }
    else {
      /* 283*/cursorColumn = cursorColumnP;
      /* 284*/return;
    }
  }

  public int getCursorRow()
      throws jpos.JposException {
    /* 287*/return cursorRow;
  }

  public void setCursorRow(int cursorRowP)
      throws jpos.JposException {
    /* 291*/int rowCount = getRows();
    /* 292*/if (cursorRowP < 0 || cursorRowP >= rowCount) {
      /* 293*/java.lang.String msg =
          java.text.MessageFormat.format("Specified cursor row \"{0}\" is out of bounds: \"{1}-{2}\"",
              new java.lang.Object[] {
              /* 293*/java.lang.String.valueOf(cursorRowP), "0", java.lang.String.valueOf(rowCount)});
      /* 297*/throw new JposException(0x6a, msg);
    }
    else {
      /* 300*/cursorRow = cursorRowP;
      /* 301*/return;
    }
  }

  public boolean getCursorUpdate()
      throws jpos.JposException {
    /* 304*/return cursorUpdate;
  }

  public void setCursorUpdate(boolean cursorUpdateP)
      throws jpos.JposException {
    /* 308*/cursorUpdate = cursorUpdateP;
  }

  public int getDeviceBrightness()
      throws jpos.JposException {
    /* 312*/return deviceBrightness;
  }

  public void setDeviceBrightness(int deviceBrightnessP)
      throws jpos.JposException {
    /* 316*/deviceBrightness = deviceBrightnessP;
  }

  public int getDeviceColumns()
      throws jpos.JposException {
    /* 320*/com.hypercom.fpe.consts.TerminalType type =
        ((com.hypercom.fpe.jpos.FPEPosService) this).getTerminalType();
    /* 321*/if (((com.hypercom.fpe.consts.BaseConstant) (type))
        .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.ICE_6000)))
        || ((com.hypercom.fpe.consts.BaseConstant) (type))
            .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.L4100)))
        || ((com.hypercom.fpe.consts.BaseConstant) (type))
            .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.L4100G)))
        || ((com.hypercom.fpe.consts.BaseConstant) (type))
            .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.L4150)))
        || ((com.hypercom.fpe.consts.BaseConstant) (type))
            .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.L4150_PCI_20)))) {
      /* 324*/return 0x2b;
    }
    /* 326*/if (((com.hypercom.fpe.consts.BaseConstant) (type))
        .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.L5300)))
        || ((com.hypercom.fpe.consts.BaseConstant) (type))
            .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.L5400)))) {
      /* 328*/return 0x57;
    }
    /* 330*/if (((com.hypercom.fpe.consts.BaseConstant) (type))
        .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.ICE_5500)))) {
      /* 332*/return 0x16;
    }
    /* 334*/if (((com.hypercom.fpe.consts.BaseConstant) (type))
        .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.HFT505)))) {
      /* 336*/return 0x14;
    }
    /* 338*/if (((com.hypercom.fpe.consts.BaseConstant) (type))
        .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.L4200)))) {
      /* 340*/return 0x1a;
    }
    /* 342*/if (((com.hypercom.fpe.consts.BaseConstant) (type))
        .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.L4250)))) {
      /* 344*/return 0x24;
    }
    /* 346*/if (((com.hypercom.fpe.consts.BaseConstant) (type))
        .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.M4100)))) {
      /* 348*/return 0x27;
    }
    else {
      /* 351*/java.lang.String msg =
          java.text.MessageFormat.format("Unknown terminal type {0}", new java.lang.Object[] {
          /* 351*/((com.hypercom.fpe.consts.BaseConstant) (type)).toString()});
      /* 353*/throw new JposException(0x6f, msg);
    }
  }

  public int getDeviceDescriptors()
      throws jpos.JposException {
    /* 357*/return 0;
  }

  public int getDeviceRows()
      throws jpos.JposException {
    /* 361*/com.hypercom.fpe.consts.TerminalType type =
        ((com.hypercom.fpe.jpos.FPEPosService) this).getTerminalType();
    /* 362*/if (((com.hypercom.fpe.consts.BaseConstant) (type))
        .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.ICE_6000)))
        || ((com.hypercom.fpe.consts.BaseConstant) (type))
            .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.L4100)))
        || ((com.hypercom.fpe.consts.BaseConstant) (type))
            .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.L4100G)))
        || ((com.hypercom.fpe.consts.BaseConstant) (type))
            .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.L4150)))
        || ((com.hypercom.fpe.consts.BaseConstant) (type))
            .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.L4150_PCI_20)))) {
      /* 365*/return 0x10;
    }
    /* 367*/if (((com.hypercom.fpe.consts.BaseConstant) (type))
        .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.L5300)))
        || ((com.hypercom.fpe.consts.BaseConstant) (type))
            .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.L5400)))) {
      /* 369*/return 0x19;
    }
    /* 371*/if (((com.hypercom.fpe.consts.BaseConstant) (type))
        .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.ICE_5500)))) {
      /* 373*/return 6;
    }
    /* 375*/if (((com.hypercom.fpe.consts.BaseConstant) (type))
        .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.HFT505)))) {
      /* 377*/return 4;
    }
    /* 379*/if (((com.hypercom.fpe.consts.BaseConstant) (type))
        .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.L4250)))) {
      /* 381*/return 9;
    }
    /* 383*/if (((com.hypercom.fpe.consts.BaseConstant) (type))
        .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.L4200)))) {
      /* 385*/return 9;
    }
    /* 387*/if (((com.hypercom.fpe.consts.BaseConstant) (type))
        .equals(((java.lang.Object) (com.hypercom.fpe.consts.TerminalType.M4100)))) {
      /* 389*/return 0x10;
    }
    else {
      /* 392*/java.lang.String msg =
          java.text.MessageFormat.format("Unknown terminal type {0}", new java.lang.Object[] {
          /* 392*/((com.hypercom.fpe.consts.BaseConstant) (type)).toString()});
      /* 394*/throw new JposException(0x6f, msg);
    }
  }

  public int getDeviceWindows()
      throws jpos.JposException {
    /* 398*/return 0;
  }

  public int getInterCharacterWait()
      throws jpos.JposException {
    /* 402*/return interCharacterWait;
  }

  public void setInterCharacterWait(int interCharacterWaitP)
      throws jpos.JposException {
    /* 407*/interCharacterWait = interCharacterWaitP;
  }

  public int getMarqueeFormat()
      throws jpos.JposException {
    /* 411*/return marqueeFormat;
  }

  public void setMarqueeFormat(int marqueeFormatP)
      throws jpos.JposException {
    /* 415*/marqueeFormat = marqueeFormatP;
  }

  public int getMarqueeRepeatWait()
      throws jpos.JposException {
    /* 419*/return marqueeRepeatWait;
  }

  public void setMarqueeRepeatWait(int marqueeRepeatWaitP)
      throws jpos.JposException {
    /* 423*/marqueeRepeatWait = marqueeRepeatWaitP;
  }

  public int getMarqueeType()
      throws jpos.JposException {
    /* 427*/return 0;
  }

  public void setMarqueeType(int marqueeType)
      throws jpos.JposException {
    /* 431*/if (0 != marqueeType) {
      /* 432*/throw new JposException(0x6a, "Only DISP_MT_NONE marquee type is supported");
    }
    else {
      /* 435*/return;
    }
  }

  public int getMarqueeUnitWait()
      throws jpos.JposException {
    /* 438*/return marqueeUnitWait;
  }

  public void setMarqueeUnitWait(int marqueeUnitWaitP)
      throws jpos.JposException {
    /* 442*/marqueeUnitWait = marqueeUnitWaitP;
  }

  public int getRows()
      throws jpos.JposException {
    /* 446*/return getDeviceRows();
  }

  public void clearDescriptors()
      throws jpos.JposException {
    /* 450*/throw new JposException(0x6a, "Descriptors are not supported in this implementation");
  }

  public void clearText()
      throws jpos.JposException {
    /* 456*/try {
      /* 456*/super.terminal.clearDisplayBuffers();
      /* 457*/clearBuffer();
      /* 458*/if (getCursorUpdate()) {
        /* 459*/setCursorRow(0);
        /* 460*/setCursorColumn(0);
      }
    }
    /* 464*/catch (com.hypercom.fpe.FPETerminalException ex) {
      /* 465*/throw new JposException(0x6f, "Error clearing display buffers", ((java.lang.Exception) (ex)));
    }
  }

  public void createWindow(int viewportRow, int viewportColumn, int viewportHeight, int viewportWidth,
      int windowHeight, int windowWidth)
      throws jpos.JposException {
    /* 474*/throw new JposException(0x6a, "Windows are not supported in this implementation");
  }

  public void destroyWindow()
      throws jpos.JposException {
    /* 479*/throw new JposException(0x6a, "Windows are not supported in this implementation");
  }

  private void clearBuffer() {
    /* 487*/for (int i = 0; i < lineBuf.length; i++ ) {
      /* 488*/byte line[] = lineBuf[i];
      /* 489*/for (int j = 0; j < line.length; j++ ) {
        /* 490*/line[j] = 0x20;
      }

    }

  }

  private void updateRow(int rowNo, com.hypercom.fpe.consts.LineDisplayStyle displayStyle)
      throws jpos.JposException {
    /* 504*/byte row[] = lineBuf[rowNo];
    /* 508*/try {
      /* 508*/super.terminal.displayLine(rowNo, 0, new String(row), getBlinkRate(), displayStyle,
          foregroundColor, backgroundColor, fontSize);
    }
    /* 517*/catch (com.hypercom.fpe.FPETerminalException ex) {
      /* 518*/throw new JposException(0x6f, "Error displaying text line", ((java.lang.Exception) (ex)));
    }
  }

  public void displayLine(java.lang.String text, int attribute)
      throws jpos.JposException {
    /* 524*/super.checkEnabled("displayText");
    /* 527*/com.hypercom.fpe.consts.LineDisplayStyle displayStyle =
        com.hypercom.fpe.consts.LineDisplayStyle.NORMAL;
    /* 528*/if (attribute == 1) {
      /* 529*/displayStyle = com.hypercom.fpe.consts.LineDisplayStyle.BLINKING;
    }
    /* 533*/byte row[] = lineBuf[getCursorRow()];
    /* 534*/int curColumn = getCursorColumn();
    /* 535*/for (int i = 0; i < text.length() && curColumn < row.length; i++ ) {
      /* 536*/row[curColumn++ ] = (byte) text.charAt(i);
    }

    /* 539*/updateRow(getCursorRow(), displayStyle);
    /* 542*/if (getCursorUpdate()) {
      /* 543*/int colCount = getColumns();
      /* 544*/int rowCount = getRows();
      /* 545*/int newRow = getCursorRow();
      /* 548*/int colAbsPos = getCursorColumn() + text.length();
      /* 549*/int newCol = colAbsPos % colCount;
      /* 553*/if (colAbsPos >= colCount) {
        /* 554*/newRow++ ;
      }
      /* 559*/if (newRow >= rowCount) {
        /* 560*/scrollUpward(newRow, attribute);
        /* 561*/newRow-- ;
      }
      /* 564*/setCursorRow(newRow);
      /* 565*/setCursorColumn(newCol);
    }
  }

  private void displayLines(java.lang.String text, int attribute)
      throws jpos.JposException {
    /* 570*/super.checkEnabled("displayText");
    /* 572*/int nlIndex = text.indexOf("\n");
    /* 573*/int crIndex = text.indexOf("\r");
    /* 575*/if (nlIndex != -1 || crIndex != -1) {
      /* 577*/int specialCharIndex =
          java.lang.Math.min(nlIndex == -1 ? 0x3e7 : nlIndex, crIndex == -1 ? 0x3e7 : crIndex);
      /* 580*/if (specialCharIndex != 0) {
        /* 582*/displayLine(text.substring(0, specialCharIndex), attribute);
      }
      /* 584*/if (nlIndex != -1 && specialCharIndex == nlIndex) {
        /* 585*/int newRow = getCursorRow() + 1;
        /* 589*/if (newRow >= getRows()) {
          /* 590*/scrollUpward(newRow, attribute);
          /* 591*/newRow-- ;
        }
        /* 593*/setCursorRow(newRow);
        /* 594*/setCursorColumn(0);
      }
      /* 596*/if (crIndex != -1 && specialCharIndex == crIndex) {
        /* 597*/setCursorColumn(0);
      }
      /* 600*/if (specialCharIndex < text.length() - "\n".length()) {
        /* 601*/displayLines(text.substring(specialCharIndex + "\n".length()), attribute);
      }
    }
    else {
      /* 607*/displayLine(text, attribute);
    }
  }

  private void scrollUpward(int newRow, int attribute)
      throws jpos.JposException {
    /* 613*/com.hypercom.fpe.consts.LineDisplayStyle displayStyle =
        com.hypercom.fpe.consts.LineDisplayStyle.NORMAL;
    /* 614*/if (attribute == 1) {
      /* 615*/displayStyle = com.hypercom.fpe.consts.LineDisplayStyle.BLINKING;
    }
    /* 619*/newRow-- ;
    /* 621*/byte oldLineBuf[][] = lineBuf;
    /* 623*/lineBuf = new byte[getDeviceRows()][getDeviceColumns()];
    /* 624*/clearBuffer();
    /* 625*/for (int i = 0; i < getRows() - 1; i++ ) {
      /* 626*/java.lang.System.arraycopy(((java.lang.Object) (oldLineBuf[i + 1])), 0,
          ((java.lang.Object) (lineBuf[i])), 0, oldLineBuf[i + 1].length);
      /* 629*/updateRow(i, displayStyle);
    }

  }

  public void displayText(java.lang.String text, int attribute)
      throws jpos.JposException {
    /* 634*/super.checkEnabled("displayText");
    /* 636*/int nStringIndex = 0;
    /* 637*/int nRemainingLength = text.length();
    /* 641*/do {
      /* 641*/int nLen = getDeviceColumns() - getCursorColumn();
      /* 642*/if (nLen > nRemainingLength) {
        /* 643*/nLen = nRemainingLength;
      }
      /* 646*/java.lang.String strTxt = new String(text.toCharArray(), nStringIndex, nLen);
      /* 647*/displayLines(strTxt, attribute);
      /* 649*/nStringIndex += nLen;
      /* 650*/nRemainingLength -= nLen;
    }
    while (nRemainingLength != 0);
  }

  public void displayTextAt(int row, int column, java.lang.String text, int attribute)
      throws jpos.JposException {
    /* 658*/super.checkEnabled("displayTextAt");
    /* 659*/int nlIndex = text.indexOf("\n");
    /* 660*/int crIndex = text.indexOf("\r");
    /* 661*/if (nlIndex != -1 || crIndex != -1) {
      /* 663*/int specialCharIndex =
          java.lang.Math.min(nlIndex == -1 ? 0x3e7 : nlIndex, crIndex == -1 ? 0x3e7 : crIndex);
      /* 666*/if (specialCharIndex != 0) {
        /* 668*/displayLineAt(row, column, text.substring(0, specialCharIndex), attribute);
      }
      /* 671*/if (nlIndex != -1 && specialCharIndex == nlIndex) {
        /* 672*/int newRow = row + 1;
        /* 676*/if (newRow >= getRows()) {
          /* 677*/scrollUpward(newRow, attribute);
          /* 678*/newRow-- ;
        }
        /* 680*/setCursorRow(newRow);
        /* 681*/setCursorColumn(0);
      }
      /* 684*/if (crIndex != -1 && specialCharIndex == crIndex) {
        /* 685*/setCursorColumn(0);
      }
      /* 688*/if (specialCharIndex < text.length() - "\n".length()) {
        /* 689*/displayTextAt(getCursorRow(), getCursorColumn(),
            text.substring(specialCharIndex + "\n".length()), attribute);
      }
    }
    else {
      /* 696*/displayLineAt(row, column, text, attribute);
    }
  }

  public void displayLineAt(int row, int column, java.lang.String text, int attribute)
      throws jpos.JposException {
    /* 702*/super.checkEnabled("displayTextAt");
    /* 704*/if (row >= getRows() || row < 0) {
      /* 705*/java.lang.String msg =
          java.text.MessageFormat.format("Row value {0} is out of bounds {1}-{2}", new java.lang.Object[] {
          /* 705*/java.lang.String.valueOf(row), "0", java.lang.String.valueOf(getRows() - 1)});
      /* 710*/throw new JposException(0x6a, msg);
    }
    /* 712*/if (column >= getColumns() || column < 0) {
      /* 713*/java.lang.String msg =
          java.text.MessageFormat.format("Column value {0} is out of bounds {1}-{2}", new java.lang.Object[] {
          /* 713*/java.lang.String.valueOf(column), "0", java.lang.String.valueOf(getColumns() - 1)});
      /* 718*/throw new JposException(0x6a, msg);
    }
    /* 722*/com.hypercom.fpe.consts.LineDisplayStyle displayStyle =
        com.hypercom.fpe.consts.LineDisplayStyle.NORMAL;
    /* 723*/if (attribute == 1) {
      /* 724*/displayStyle = com.hypercom.fpe.consts.LineDisplayStyle.BLINKING;
    }
    /* 728*/byte rowBuf[] = lineBuf[row];
    /* 729*/int curColumn = column;
    /* 730*/for (int i = 0; i < text.length() && curColumn < rowBuf.length; i++ ) {
      /* 731*/rowBuf[curColumn++ ] = (byte) text.charAt(i);
    }

    /* 734*/updateRow(row, displayStyle);
    /* 737*/if (getCursorUpdate()) {
      /* 738*/int colCount = getColumns();
      /* 739*/int rowCount = getRows();
      /* 740*/int newRow = row;
      /* 743*/int colAbsPos = column + text.length();
      /* 744*/int newCol = colAbsPos % colCount;
      /* 748*/if (colAbsPos >= colCount) {
        /* 749*/newRow++ ;
      }
      /* 758*/if (newRow >= rowCount) {
        /* 759*/scrollUpward(newRow, attribute);
        /* 760*/newRow-- ;
      }
      /* 763*/setCursorRow(newRow);
      /* 764*/setCursorColumn(newCol);
    }
  }

  public void refreshWindow(int window)
      throws jpos.JposException {
    /* 769*/throw new JposException(0x6a, "Windows are not supported in this implementation");
  }

  public void scrollText(int direction, int units)
      throws jpos.JposException {
    /* 774*/throw new JposException(0x6a, "Scroll is not supported in this implementation");
  }

  public void setDescriptor(int descriptor, int attribute)
      throws jpos.JposException {
    /* 779*/throw new JposException(0x6a, "Descriptors are not supported in this implementation");
  }

  public void open(java.lang.String logicalName, jpos.services.EventCallbacks cb)
      throws jpos.JposException {
    /* 784*/super.open(logicalName, cb);
    /* 785*/cursorUpdate = true;
    /* 786*/cursorColumn = 0;
    /* 787*/cursorRow = 0;
    /* 788*/deviceBrightness = 0x64;
    /* 789*/interCharacterWait = 0;
    /* 790*/blinkRate = 0x3de;
    /* 793*/foregroundColor = super.getIntProperty("foregroundColor", 0);
    /* 794*/backgroundColor = super.getIntProperty("backgroundColor", 0xff);
    /* 795*/int nFontSize = super.getIntProperty("fontSize", 0);
    /* 796*/fontSize =
        com.hypercom.fpe.consts.LineDisplayFontSize.getInstance(java.lang.Character.forDigit(nFontSize, 0xa));
    /* 799*/lineBuf = new byte[getDeviceRows()][getDeviceColumns()];
    /* 800*/clearBuffer();
  }

  protected boolean isEnqueueEvent(java.util.EventObject event) {
    /* 813*/return (event instanceof com.hypercom.fpe.event.EvtDirectIO) && super.isDirectIOEventExpected
        || (event instanceof com.hypercom.fpe.event.EvtPowerState) && 1 == super.powerNotify;
  }

  protected jpos.events.JposEvent processEvent(java.util.EventObject event) {
    /* 837*/return null;
  }

  public void directIO(int command, int data[], java.lang.Object object)
      throws jpos.JposException {
    /* 850*/super.isDirectIOEventExpected = true;
    /* 852*/java.lang.StringBuffer strCommand = new StringBuffer();
    String s = (String) object;
    /* 853*/for (int i = 0; i < data.length; i++ ) {
      /* 854*/strCommand.append((char) data[i]);
    }

    /* 858*/try {
      super.terminal.SendData(s);
      /* 858*/// super.terminal.SendData(strCommand.substring(0, strCommand.length()));
    }
    /* 860*/catch (com.hypercom.fpe.FPETerminalException ex) {
      /* 861*/throw new JposException(0x6f, "Error directIO ", ((java.lang.Exception) (ex)));
    }
  }

  protected com.hypercom.fpe.consts.BaseConstant[] getManufacturerSpecificStatistics() {
    /* 869*/if (super.manufacturerStatistics == null) {
      /* 870*/com.hypercom.fpe.consts.BaseConstant dest[] =
          new com.hypercom.fpe.consts.BaseConstant[com.hypercom.stat.LineDisplayStatistic.M_ELEMENTS.length
              + com.hypercom.stat.CommonStatistic.M_ELEMENTS.length];
      /* 873*/com.hypercom.util.Logger.logDebug(java.util.Arrays
          .toString(((java.lang.Object[]) (com.hypercom.stat.CommonStatistic.M_ELEMENTS))));
      /* 874*/com.hypercom.util.Logger.logDebug(java.util.Arrays
          .toString(((java.lang.Object[]) (com.hypercom.stat.LineDisplayStatistic.M_ELEMENTS))));
      /* 877*/java.lang.System.arraycopy(
          ((java.lang.Object) (com.hypercom.stat.CommonStatistic.M_ELEMENTS)), 0,
          ((java.lang.Object) (dest)), 0, com.hypercom.stat.CommonStatistic.M_ELEMENTS.length);
      /* 881*/java.lang.System.arraycopy(
          ((java.lang.Object) (com.hypercom.stat.LineDisplayStatistic.M_ELEMENTS)), 0,
          ((java.lang.Object) (dest)), com.hypercom.stat.CommonStatistic.M_ELEMENTS.length,
          com.hypercom.stat.LineDisplayStatistic.M_ELEMENTS.length);
      /* 884*/java.util.Arrays.sort(((java.lang.Object[]) (dest)));
      /* 885*/super.manufacturerStatistics = dest;
    }
    /* 887*/return super.manufacturerStatistics;
  }

  protected com.hypercom.fpe.consts.BaseConstant[] getUPOSStatistics() {
    /* 894*/if (super.uposStatistics == null) {
      /* 896*/com.hypercom.fpe.consts.BaseConstant dest[] =
          new com.hypercom.fpe.consts.BaseConstant[com.hypercom.stat.LineDisplayStatistic.U_ELEMENTS.length
              + com.hypercom.stat.CommonStatistic.U_ELEMENTS.length];
      /* 899*/com.hypercom.util.Logger.logDebug(java.util.Arrays
          .toString(((java.lang.Object[]) (com.hypercom.stat.CommonStatistic.U_ELEMENTS))));
      /* 900*/com.hypercom.util.Logger.logDebug(java.util.Arrays
          .toString(((java.lang.Object[]) (com.hypercom.stat.LineDisplayStatistic.U_ELEMENTS))));
      /* 903*/java.lang.System.arraycopy(
          ((java.lang.Object) (com.hypercom.stat.CommonStatistic.U_ELEMENTS)), 0,
          ((java.lang.Object) (dest)), 0, com.hypercom.stat.CommonStatistic.U_ELEMENTS.length);
      /* 907*/java.lang.System.arraycopy(
          ((java.lang.Object) (com.hypercom.stat.LineDisplayStatistic.U_ELEMENTS)), 0,
          ((java.lang.Object) (dest)), com.hypercom.stat.CommonStatistic.U_ELEMENTS.length,
          com.hypercom.stat.LineDisplayStatistic.U_ELEMENTS.length);
      /* 910*/java.util.Arrays.sort(((java.lang.Object[]) (dest)));
      /* 911*/super.uposStatistics = dest;
    }
    /* 913*/return super.uposStatistics;
  }

  protected com.hypercom.fpe.consts.BaseConstant getDeviceCategorySpecificStatistic(java.lang.String name) {
    /* 921*/return ((com.hypercom.fpe.consts.BaseConstant) (com.hypercom.stat.LineDisplayStatistic
        .getInstance(name)));
  }
}
