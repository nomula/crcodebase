@echo off
REM $Id$
REM $URL$
REM Determine the relative path of the current directory to
REM the script, and then change to that directory.
set REL_DIR=%~dp0
pushd %REL_DIR%

setlocal

REM **  Set the xstore home directory
set XST_HOME=%REL_DIR%
%XST_HOME%/windows/jre/bin/java.exe -cp lib/dtv-upgrader.jar;lib/dtv-util.jar;lib/ext/log4j.jar;lib/ext/xercesImpl.jar;lib/ext/xml-apis.jar dtv.installer.util.PropertyMerge updates/base-xstore.properties updates/prop-map.xml true  

%XST_HOME%/windows/jre/bin/java.exe -cp lib/dtv-upgrader.jar;lib/dtv-util.jar;lib/ext/log4j.jar;lib/ext/xercesImpl.jar;lib/ext/xml-apis.jar dtv.installer.util.PropertyMerge updates/xstore.properties updates/prop-map.xml true  
