#********************************************************************
# Wrapper Java Properties
#********************************************************************

#include ../conf/security.conf
#include ../conf/params.conf
#include ../conf/include-windows.conf

# working directory, relative to the wrapper.exe that is used to interpret 
#  all other paths (except #include directives) and used as the working 
#  directory when running the application
wrapper.working.dir=../../

set.default.RUNTIME_DIR=windows

# Java Application
wrapper.java.command=../runtime/%RUNTIME_DIR%/jre/bin/envui

# Java Main class.  This class must implement the WrapperListener interface
#  or guarantee that the WrapperManager class is initialized.  Helper
#  classes are provided to do this for you.  See the Integration section
#  of the documentation for details.
wrapper.java.mainclass=org.tanukisoftware.wrapper.WrapperSimpleApp

# Configure PID and Anchor files
wrapper.anchorfile=../tmp/xenv_ui.anchor
wrapper.java.pidfile=../tmp/xenv_ui.pid

# Java Classpath (include wrapper.jar)  Add class path elements as needed starting from 1
# Do not add the plugin folder; they are assigned to the classpath dynamically in code after being loaded. This will cause only the first jar to be loaded.
wrapper.java.classpath.1=wrapper/lib/wrapper.jar
wrapper.java.classpath.2=lib/patch/*.jar
wrapper.java.classpath.3=lib/*.jar
wrapper.java.classpath.4=lib/ext/*.jar
wrapper.java.classpath.5=lib/ext/jna/*.jar
wrapper.java.classpath.6=lib/ext/scripting/*.jar
wrapper.java.classpath.7=lib/ext/hibernate/*.jar
wrapper.java.classpath.8=lib/ext/jcron-lib/*.jar
wrapper.java.classpath.9=lib/ext/derby/*.jar
wrapper.java.classpath.10=lib/ext/jfreechart/*.jar
wrapper.java.classpath.11=lib/ext/mssql2k5/*.jar
wrapper.java.classpath.12=lib/ext/oracle/*.jar
wrapper.java.classpath.13=cust_config
wrapper.java.classpath.14=config
wrapper.java.classpath.15=res

# Java Library Path (location of Wrapper.DLL or libwrapper.so)
wrapper.java.library.path.1=wrapper/lib


# Java Additional Parameters
wrapper.java.additional.1=-Djavax.net.ssl.trustStore=res/ssl/.truststore
wrapper.java.additional.2=-Ddtv.util.net.SslUtils.ContextType=TLSv1.2
wrapper.java.additional.3=-Ddtv.util.net.SslUtils.BlacklistedProtocols=SSLv2,SSLv2Hello,SSLv3,TLSv1,TLSv1.1
#wrapper.java.additional.2=-Duser.timezone=America/Los_Angeles
#wrapper.java.additional.3=-Xdebug
#wrapper.java.additional.4=-Xrunjdwp:transport=dt_socket,address=8000,server=y

# Initial Java Heap Size (in MB)
wrapper.java.initmemory=16

# Maximum Java Heap Size (in MB)
wrapper.java.maxmemory=128

# Application parameters.  Add parameters as needed starting from 1
wrapper.app.parameter.1=dtv.env.app.xEnvMain

#********************************************************************
# Wrapper Logging Properties
#********************************************************************
# Allow for the use of non-contiguous numbered properties
wrapper.ignore_sequence_gaps=TRUE

# Enables Debug output from the Wrapper.
# wrapper.debug=TRUE

# Format of output for the console.  (See docs for formats)
wrapper.console.format=PM

# Log Level for console output.  (See docs for log levels)
wrapper.console.loglevel=INFO

# Log file to use for wrapper output logging.
wrapper.logfile=log/wrapper.log

# Format of output for the log file.  (See docs for formats)
wrapper.logfile.format=LPTM

# Log Level for log file output.  (See docs for log levels)
wrapper.logfile.loglevel=ERROR

# Maximum size that the log file will be allowed to grow to before
#  the log is rolled. Size is specified in bytes.  The default value
#  of 0, disables log rolling.  May abbreviate with the 'k' (kb) or
#  'm' (mb) suffix.  For example: 10m = 10 megabytes.
wrapper.logfile.maxsize=10m

# Maximum number of rolled log files which will be allowed before old
#  files are deleted.  The default value of 0 implies no limit.
wrapper.logfile.maxfiles=10

# Log Level for sys/event log output.  (See docs for log levels)
wrapper.syslog.loglevel=NONE

#********************************************************************
# Wrapper General Properties
#********************************************************************

# Title to use when running as a console
wrapper.console.title=xenv_ui

# set the JVM Ping Timeout to 60 (default=30); same as Xstore fixes PTS #fb210923, 214919
wrapper.ping.timeout=90

# Default JVM ping interval
wrapper.ping.interval=5

# Command file; file is monitored by the wrapper to execute commands.
wrapper.commandfile=wrapper/conf/xenvironment.command
wrapper.command.poll_interval=5
